
import resource
import json
import hidden
import twitter
import re
ckey = ''
cscr = ''
atoken = ''
atokscr = ''


def load():
    print('...')
    print('...')


api = twitter.Api(sleep_on_rate_limit=True, consumer_key=ckey,
                  consumer_secret=cscr,
                  access_token_key=atoken, access_token_secret=atokscr,
                  )
uname = input('Enter a Twitter Username: ')

print('...')
print('Retrieving', uname, 'profile...')
load()
profile = api.GetUser(screen_name=uname, return_json=True)
print('>>> Basic Details <<<')
print('...')
print('Name: ', profile['name'])
print('Handle: @',profile['screen_name'])
print('Location: ', profile['location'])
print('Following: ', profile['friends_count'], ' ', 'Followers: ',
      profile['followers_count'])
print('Description: ', profile['description'])
print('Profile URL: ', profile['url'])
load()

user_tline = api.GetUserTimeline(screen_name=uname, count=5)
print('>>> Retrieving 5 Latest Tweets from: <<<', uname)
load()
count = 0
for status in user_tline:
    count = count + 1
    print(count,')')
    print(status.text)
    print("")

#resource usage
usageMB = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 2 ** 20
print('Resources used: ',usageMB, 'MB')
